﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Column01 : MonoBehaviour {

    private void OnTriggerEnter2D (Collider2D other)
    {
        if (other.GetComponent<Bird>() != null)
        {
            GameControl01.instance.BirdScored(); 
        }
    }
}
